import java.util.Scanner;

public class Main {

	// Check if input is integer or not
	public static boolean checkInt(String input) {
		try {
			Integer.parseInt(input);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public static void main(String[] args) {
		Enigma enigmaMachine = new Enigma();

		Scanner reader = new Scanner(System.in);
		System.out.println("Do you want sample run(y/n): ");
		String input = reader.nextLine();
		if (input.contentEquals("y")) {

			// Here are some example run
			// It decode the other encode
			String messageOne = "helloworld1923";
			System.out.println("Message: " + messageOne + "\n");
			String encoded = enigmaMachine.encode(messageOne, "2", "2", "3");
			System.out.println("Encoded: " + encoded + "\n");
			String decoded = enigmaMachine.decode(encoded);
			System.out.println("Decoded: " + decoded + "\n");

			String messageTwo = "thisisthetest1212";
			System.out.println("Message: " + messageTwo + "\n");
			String encodedTwo = enigmaMachine.encode(messageTwo, "10", "3", "7");
			System.out.println("Encoded: " + encodedTwo + "\n");
			String decodedTwo = enigmaMachine.decode(encodedTwo);
			System.out.println("Decoded: " + decodedTwo + "\n");

			String messageThree = "attackwest145";
			System.out.println("Message: " + messageThree + "\n");
			String encodedThree = enigmaMachine.encode(messageThree, "5", "2", "1");
			System.out.println("Encoded: " + encodedThree + "\n");
			String decodedThree = enigmaMachine.decode(encodedThree);
			System.out.println("Decoded: " + decodedThree + "\n");

		} else if (input.contentEquals("n")) {

			boolean flag = true;
			while (flag) {
				System.out.println("Do you want to encode or decode message (e/d) or quit (q): ");
				input = reader.nextLine();

				if (input.contentEquals("e")) {
					System.out.println("Type your message with wheel position (ex: hello 1 2 3): ");
					input = reader.nextLine();
					String[] count = input.split(" ");
					while (count.length != 4 || !checkInt(count[1]) && !checkInt(count[2]) && !checkInt(count[3])) {
						System.out.println("Invalid Input. Re-enter: ");
						input = reader.nextLine();
						count = input.split(" ");
					}
					System.out.println("Message: " + count[0] + "\n");
					String encoded = enigmaMachine.encode(count[0], count[1], count[2], count[3]);
					System.out.println("Encoded: " + encoded + "\n");
				} else if (input.contentEquals("d")) {

					System.out.println("Type your encoded message with wheel position (ex: fsuafi 5 3 1): ");
					input = reader.nextLine();
					String[] count = input.split(" ");
					while (count.length != 4 || !checkInt(count[1]) && !checkInt(count[2]) && !checkInt(count[3])) {
						System.out.println("Invalid Input. Re-enter: ");
						input = reader.nextLine();
						count = input.split(" ");
					}
					System.out.println("Message: " + count[0] + "\n");
					String encoded = enigmaMachine.decode(input);
					System.out.println("Encoded: " + encoded + "\n");

				} else if (input.contentEquals("q")) {
					System.out.println("Quit Enigma\n");
					break;
				} else {
					System.out.println("Invalid Input\n");
				}
			}

		} else {
			System.out.println("Invalid input");
		}

		reader.close();

	}

}
