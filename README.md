# Enigma Machine

<h2>Introduction</h2>
The Enigma Simulator is built base on the real enigma machine with some modifications:<br>
+Allows letters and digits (mod 36).<br/>
+Wheels are in same order.<br/>
+Plugboard and reflector operates as static wheel.<br/>
+Rotating wheel: right 1/character; middle 1/ 7 characters; left 1/ 5 characters.<br/>

<h2>Classes</h2>
Wheel: using to declare and rotate array as wheels. <br />
Enigma: declare values of wheels, encode and decode. <br />
Main: has sample run or user input to encode or decode. <br />

<h2>Operations</h2>
Encode: plugboard -> right -> middle -> left -> reflector -> left -> middle -> right -> plugboard.<br />
Decode: plugboard -> right -> middle -> left -> reflector -> left -> middle -> right -> plugboard.<br />
Each one has two columns: first is the wiring values of wheels and second is a-9 values of wheels. They operates accordingly if wheels rotate.<br />

<h2>Instructions</h2>
_EnigmaSimulator_eclispe: import to eclipse to run the files.<br /> 
_EnigmaSimilator_terminal: use makefile to run.<br />
_make all: compile run and clean folder _make compile: compile files.<br>
_make run: run Main file.<br>
_make clean: clean all *.class files.<br>

<h2>Demo<h2>
![Sample_One](Demo/sample_1.png)
<br>
![Sample_Two](Demo/sample_2.png)