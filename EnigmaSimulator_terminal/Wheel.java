
public class Wheel {

	private String[] current;

	public Wheel(String[] start) {
		this.current = start;
	}

	public String[] getWheel() {
		// System.out.println(current[0]);
		return this.current;
	}

	// Rotate wheel left one time
	public void rotateWheelLeft() {
		String[] rotateLeft = new String[this.current.length];
		for (int x = 0; x <= current.length - 1; x++) {
			rotateLeft[(x + (current.length - 1)) % current.length] = current[x];
		}
		this.current = rotateLeft;
	}

	// Rotate wheel right one time
	public void rotateWheelRight() {
		String[] rotateRight = new String[this.current.length];
		for (int x = 0; x < current.length; x++) {
			rotateRight[(x + 1) % current.length] = current[x];
		}
		this.current = rotateRight;
	}

}
