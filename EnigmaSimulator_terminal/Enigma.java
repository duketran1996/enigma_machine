import java.util.Arrays;

public class Enigma {

	// Declare value of original left middle and right wheel
	private String[] original = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q",
			"r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };

	private String[] left = { "2", "y", "z", "0", "1", "a", "w", "i", "p", "k", "s", "n", "3", "t", "e", "r", "m", "u",
			"c", "5", "v", "6", "x", "7", "f", "q", "o", "l", "4", "8", "g", "d", "9", "b", "j", "h" };

	private String[] middle = { "0", "l", "x", "1", "2", "8", "h", "b", "3", "n", "r", "o", "k", "d", "t", "7", "c",
			"6", "p", "i", "v", "j", "4", "a", "u", "w", "m", "e", "9", "5", "q", "s", "z", "g", "y", "f" };

	private String[] right = { "3", "5", "h", "e", "f", "g", "d", "q", "8", "m", "2", "k", "l", "j", "n", "s", "u", "w",
			"o", "v", "r", "x", "z", "c", "i", "9", "t", "7", "b", "p", "a", "0", "1", "y", "6", "4" };

	// Plugboard and reflector are static wheel
	private String[] plugboard = { "z", "9", "4", "o", "y", "m", "l", "5", "t", "j", "d", "x", "i", "n", "8", "1", "w",
			"g", "3", "r", "f", "e", "0", "u", "b", "s", "a", "6", "q", "v", "k", "h", "c", "7", "2", "p" };

	private String[] reflector = { "i", "a", "q", "7", "w", "1", "y", "4", "n", "s", "l", "u", "x", "v", "6", "3", "z",
			"b", "j", "t", "2", "8", "r", "9", "d", "f", "k", "5", "c", "o", "e", "h", "0", "m", "p", "g" };

	// Declare wheel for wiring and encode the message
	// Wheel comes in pair with original wheel which are a-9 values
	private Wheel rightWheel = new Wheel(right);

	private Wheel rightWheelOriginal = new Wheel(original);

	private Wheel middleWheel = new Wheel(middle);

	private Wheel middleWheelOriginal = new Wheel(original);

	private Wheel leftWheel = new Wheel(left);

	private Wheel leftWheelOriginal = new Wheel(original);

	// Get the index of the letter in the wheel
	public int getIndex(String letter, String[] wheel) {
		return Arrays.asList(wheel).indexOf(letter);
	}

	// Get the letter from the founded index
	public String getLetter(String letter, String[] wheelOne, String[] wheelTwo) {
		return wheelOne[getIndex(letter, wheelTwo)];
	}

	// Start Encoding
	// The method takes a message and the position of wheels
	public String encode(String message, String rightTurn, String middleTurn, String leftTurn) {

		// Declare counting variables
		String encodedMessage = "";
		int countRight = 0;
		int countMiddle = 0;
		int countLeft = 0;

		// Start rotating the wheel before encoding
		for (int i = 0; i < Integer.parseInt(rightTurn); i++) {
			rightWheel.rotateWheelLeft();
			rightWheelOriginal.rotateWheelLeft();
		}

		for (int i = 0; i < Integer.parseInt(middleTurn); i++) {
			middleWheel.rotateWheelLeft();
			middleWheelOriginal.rotateWheelLeft();
		}

		for (int i = 0; i < Integer.parseInt(leftTurn); i++) {
			leftWheel.rotateWheelLeft();
			leftWheelOriginal.rotateWheelLeft();
		}

		for (char letter : message.toCharArray()) {
			countRight++;

			// Rotate Wheel
			rightWheel.rotateWheelLeft();
			rightWheelOriginal.rotateWheelLeft();

			// Rotate middle wheel every 7 characters
			if ((countRight % 7) == 0) {
				middleWheel.rotateWheelLeft();
				middleWheelOriginal.rotateWheelLeft();
				countMiddle++;
			}

			// Rotate left wheel every 5 characters
			if ((countRight % 5) == 0) {
				leftWheel.rotateWheelLeft();
				leftWheelOriginal.rotateWheelLeft();
				countLeft++;
			}

			// Letter will go through wiring encode.
			// The flow: plugboard -> right -> middle -> left -> reflector -> left -> middle
			// -> right-> plugboard
			String plugLetter = getLetter(String.valueOf(letter), plugboard, original);

			String rightLetter = getLetter(plugLetter, rightWheel.getWheel(), original);

			String middleLetter = getLetter(rightLetter, middleWheel.getWheel(), rightWheelOriginal.getWheel());

			String leftLetter = getLetter(middleLetter, leftWheel.getWheel(), middleWheelOriginal.getWheel());

			String reflectorLetter = getLetter(leftLetter, reflector, leftWheelOriginal.getWheel());

			String leftLetterBack = getLetter(reflectorLetter, middleWheelOriginal.getWheel(), leftWheel.getWheel());

			String middleLetterBack = getLetter(leftLetterBack, rightWheelOriginal.getWheel(), middleWheel.getWheel());

			String rightLetterBack = getLetter(middleLetterBack, original, rightWheel.getWheel());

			String plugBack = getLetter(rightLetterBack, plugboard, leftWheelOriginal.getWheel());

			encodedMessage += plugBack;
		}

		// Return the encoded message with position of wheels for decode
		return String.format("%s %s %s %s", encodedMessage, countRight, countMiddle, countLeft);
	}

	// Start decoding
	// Take in the encoding message
	public String decode(String message) {

		// Declare variable for decoded message and count for wheel rotation
		String decodedMessage = "";
		int count = 0;

		// Message should have the encoded and positions of wheels
		String[] messagePart = message.split(" ");

		// Rotate the wheel to correct position
		for (int i = 0; i < Integer.parseInt(messagePart[1]); i++) {
			rightWheel.rotateWheelRight();
			rightWheelOriginal.rotateWheelRight();
		}

		for (int i = 0; i < Integer.parseInt(messagePart[2]); i++) {
			middleWheel.rotateWheelRight();
			middleWheelOriginal.rotateWheelRight();
		}

		for (int i = 0; i < Integer.parseInt(messagePart[3]); i++) {
			leftWheel.rotateWheelRight();
			leftWheelOriginal.rotateWheelRight();
		}

		for (char letter : messagePart[0].toCharArray()) {
			count++;

			// Rotate Wheel
			rightWheel.rotateWheelLeft();
			rightWheelOriginal.rotateWheelLeft();

			// Rotate middle wheel every 7 characters
			if ((count % 7) == 0) {
				middleWheel.rotateWheelLeft();
				middleWheelOriginal.rotateWheelLeft();
			}

			// Rotate left wheel every 5 characters
			if ((count % 5) == 0) {
				leftWheel.rotateWheelLeft();
				leftWheelOriginal.rotateWheelLeft();
			}

			// Letter will go through wiring decode.
			// The flow: plugboard -> right -> middle -> left -> reflector -> left -> middle
			// -> right-> plugboard
			String plugBack = getLetter(String.valueOf(letter), leftWheelOriginal.getWheel(), plugboard);

			String rightLetter = getLetter(plugBack, rightWheel.getWheel(), original);

			String middleLetter = getLetter(rightLetter, middleWheel.getWheel(), rightWheelOriginal.getWheel());

			String leftLetter = getLetter(middleLetter, leftWheel.getWheel(), middleWheelOriginal.getWheel());

			String reflectorLetter = getLetter(leftLetter, leftWheelOriginal.getWheel(), reflector);

			String leftLetterBack = getLetter(reflectorLetter, middleWheelOriginal.getWheel(), leftWheel.getWheel());

			String middleLetterBack = getLetter(leftLetterBack, rightWheelOriginal.getWheel(), middleWheel.getWheel());

			String rightLetterBack = getLetter(middleLetterBack, original, rightWheel.getWheel());

			String plugLetter = getLetter(rightLetterBack, original, plugboard);

			decodedMessage += plugLetter;

		}
		return decodedMessage;
	}

	// A backward trace of encode for debug and understanding
	// Unused
	public String decodeSample(String message) {

		String[] messagePart = message.split(" ");

		String decodedMessage = "";
		int count = messagePart[0].length() + 1;
		for (char letter : messagePart[0].toCharArray()) {
			count--;
			String plugBack = getLetter(String.valueOf(letter), leftWheelOriginal.getWheel(), plugboard);

			String rightLetter = getLetter(plugBack, rightWheel.getWheel(), original);

			String middleLetter = getLetter(rightLetter, middleWheel.getWheel(), rightWheelOriginal.getWheel());

			String leftLetter = getLetter(middleLetter, leftWheel.getWheel(), middleWheelOriginal.getWheel());

			String reflectorLetter = getLetter(leftLetter, leftWheelOriginal.getWheel(), reflector);

			String leftLetterBack = getLetter(reflectorLetter, middleWheelOriginal.getWheel(), leftWheel.getWheel());

			String middleLetterBack = getLetter(leftLetterBack, rightWheelOriginal.getWheel(), middleWheel.getWheel());

			String rightLetterBack = getLetter(middleLetterBack, original, rightWheel.getWheel());

			// Rotate Wheel
			if ((count % 7) == 0) {
				middleWheel.rotateWheelRight();
				middleWheelOriginal.rotateWheelRight();
			}

			if ((count % 5) == 0) {
				leftWheel.rotateWheelRight();
				leftWheelOriginal.rotateWheelRight();
			}

			rightWheel.rotateWheelRight();
			rightWheelOriginal.rotateWheelRight();

			String plugLetter = getLetter(rightLetterBack, original, plugboard);

			decodedMessage += plugLetter;
		}
		return decodedMessage;
	}

}
